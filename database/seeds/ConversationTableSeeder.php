<?php

use Illuminate\Database\Seeder;
use App\Conversation;

class ConversationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      // conversariones user 1

        Conversation::create([
          'user_id' => 1,
          'contact_id' => 2,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 1,
          'contact_id' => 3,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 1,
          'contact_id' => 4,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 1,
          'contact_id' => 5,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 1,
          'contact_id' => 6,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 1,
          'contact_id' => 7,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 1,
          'contact_id' => 8,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 1,
          'contact_id' => 9,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        // conversariones user 2

        Conversation::create([
          'user_id' => 2,
          'contact_id' => 1,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 2,
          'contact_id' => 3,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 2,
          'contact_id' => 4,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 2,
          'contact_id' => 5,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        // conversariones user 3

        Conversation::create([
          'user_id' => 3,
          'contact_id' => 1,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 3,
          'contact_id' => 2,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 3,
          'contact_id' => 4,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        // conversariones user 4

        Conversation::create([
          'user_id' => 4,
          'contact_id' => 1,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 4,
          'contact_id' => 2,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 4,
          'contact_id' => 3,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        // conversariones user 5

        Conversation::create([
          'user_id' => 5,
          'contact_id' => 1,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 5,
          'contact_id' => 2,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 5,
          'contact_id' => 9,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        // conversariones user 6

        Conversation::create([
          'user_id' => 6,
          'contact_id' => 1,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 6,
          'contact_id' => 9,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        // conversariones user 7

        Conversation::create([
          'user_id' => 7,
          'contact_id' => 1,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 7,
          'contact_id' => 9,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        // conversariones user 8

        Conversation::create([
          'user_id' => 8,
          'contact_id' => 1,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 8,
          'contact_id' => 9,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        // conversariones user 9

        Conversation::create([
          'user_id' => 9,
          'contact_id' => 1,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 9,
          'contact_id' => 5,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 9,
          'contact_id' => 6,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 9,
          'contact_id' => 7,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);

        Conversation::create([
          'user_id' => 9,
          'contact_id' => 8,
          'last_message' => null,
          'last_time' => null,
          // 'listen_notifications' => true,
          // 'has_blocked' => false,
        ]);
    }
}

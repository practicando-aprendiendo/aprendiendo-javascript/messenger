<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
          'name' => 'Sergio Benavides',
          'email' => 'checho.ch55ll@correo.com',
          'password' => bcrypt('123123')
        ]);

        User::create([
          'name' => 'La iama machetiada',
          'email' => 'iama.machetiada@correo.com',
          'password' => bcrypt('123123')
        ]);

        User::create([
          'name' => 'artico es noob',
          'email' => 'artico.noob@correo.com',
          'password' => bcrypt('123123')
        ]);

        User::create([
          'name' => 'prodigy es larva',
          'email' => 'prodigy.larva@correo.com',
          'password' => bcrypt('123123')
        ]);

        User::create([
          'name' => 'Luis quiñones',
          'email' => 'luis.chimpe@correo.com',
          'password' => bcrypt('123123')
        ]);

        User::create([
          'name' => 'Sebastian Giraldo',
          'email' => 'sebas.giraldo@correo.com',
          'password' => bcrypt('123123')
        ]);

        User::create([
          'name' => 'Wilder Palacios',
          'email' => 'wilder.palacios@correo.com',
          'password' => bcrypt('123123')
        ]);

        User::create([
          'name' => 'Daniel Andres Gomez',
          'email' => 'daniel.gomez@correo.com',
          'password' => bcrypt('123123')
        ]);

        User::create([
          'name' => 'usuario de prueba',
          'email' => 'user.prueba@correo.com',
          'password' => bcrypt('123123')
        ]);
    }
}
